//
//  Current.swift
//  Quotes
//
//  Created by Chinedu Amadi-Ndukwe on 04/12/2014.
//  Copyright (c) 2014 Chinedu Amadi-Ndukwe. All rights reserved.
//

import Foundation
import UIKit

struct Current {
    var currentA: String
    var name: String
    
    init(quoteDictionary:NSDictionary) {
        currentA = quoteDictionary["quote"] as String
        name = quoteDictionary["name"] as String
    }
}