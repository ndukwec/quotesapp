//
//  ViewController.swift
//  Quotes
//
//  Created by Chinedu Amadi-Ndukwe on 04/12/2014.
//  Copyright (c) 2014 Chinedu Amadi-Ndukwe. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    var arrayQuote = [Current]()
    var favQuote = [Current]()

    @IBOutlet weak var quoteLabel: UILabel!
    
    @IBOutlet weak var authorLabel: UILabel!
    
    @IBOutlet weak var refreshButton: UIButton!
    
    @IBOutlet weak var imageImage: UIImageView!
    
    @IBOutlet weak var refreshQuote: UIActivityIndicatorView!
    
   
    var currentIndex: Int = 0;
    
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //refreshQuote.hidden = true
        getQuote()
   
    }
    
    
    func getQuote() -> Void {
        let baseurl = NSURL(string: "https:blistering-inferno-5873.firebaseio.com/Quotes.json")
        
        var request = NSURLRequest(URL: baseurl!)
        
        let sessionConfiguration = NSURLSessionConfiguration.defaultSessionConfiguration()
        
        let session = NSURLSession(configuration: sessionConfiguration)
        
        session.dataTaskWithRequest(request, completionHandler: {(data, response, error) in
            
            if (error != nil) {
                println(error.localizedDescription)
            }
            else {
                var response = NSString(data: data, encoding: NSUTF8StringEncoding)
                println(response!)
                
                var jsonError : NSError?
                let quotesArray : NSArray? = NSJSONSerialization.JSONObjectWithData(data, options: nil, error: &jsonError) as? NSArray
                
                
                if (jsonError != nil) {
                    println(jsonError?.userInfo)
                }
                else {
                    
                    for var i = 0; i < quotesArray?.count; i++ {
                        if let quote = quotesArray?.objectAtIndex(i) as? NSDictionary {
                            let currentQuote = Current(quoteDictionary: quote)
                            self.arrayQuote.append(currentQuote)
                        }
                    }
                    
                    
                    dispatch_async(dispatch_get_main_queue(), { () -> Void in
                        
                        if self.arrayQuote.count > 0 {
                            let firstQuote = self.arrayQuote[0]
                            self.quoteLabel.text = "\(firstQuote.currentA)"
                            
                            self.authorLabel.text = "\(firstQuote.name)"
                            
                            self.refreshQuote.stopAnimating()
                            self.refreshQuote.hidden = true
                           // self.refreshButton.hidden = false
                        }
                    })
                }

            }
            
        }).resume()
        
        
        
        
        
}

    
    
    @IBAction func refreshButton(sender: AnyObject) {
    
        if(currentIndex + 1 != arrayQuote.count){
            currentIndex++;
        } else {
            currentIndex = 0;
        }
           self.quoteLabel.text = arrayQuote[currentIndex].currentA
           self.authorLabel.text = arrayQuote[currentIndex].name
        
         changeBackground()
    }
    
    
  func changeBackground() -> UIImageView {
    
    
       if authorLabel.text == "Albert Einstein" {
            imageImage.image = UIImage(named: "Albert_Einstein.png")
        }
        else if authorLabel.text == "Bertrand Russell"  {
        imageImage.image = UIImage(named:"bertrand-russell.jpg")
        }
        else if authorLabel.text == "Napoleon Bonaparte" {
        imageImage.image = UIImage(named:"npb.jpeg")
        }
    
        else if authorLabel.text == "H. G. Wells" {
            imageImage.image = UIImage(named:"hg_wells.jpg")
        }
        else if authorLabel.text == "Sir Winston Churchill" {
            imageImage.image = UIImage(named: "Winston_Churchill.jpg")
        }
    
        return imageImage
    
}
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        println("This is being called")

        if let destination = segue.destinationViewController as? TableViewController {
            if segue.identifier == "showQuote"{
            destination.quotes = favQuote
                println("this is being called now")
            }
        }
    }
    

    @IBAction func addFav(sender: AnyObject) {
        favQuote.append(arrayQuote[currentIndex])
            println("Added to \(favQuote)")
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

